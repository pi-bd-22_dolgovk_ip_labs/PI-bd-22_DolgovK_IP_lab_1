package ru.ulstu.is.sbapp;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.ArrayList;
import java.util.Random;

@SpringBootApplication
@RestController
public class SbappApplication {

	public static void main(String[] args) {
		SpringApplication.run(SbappApplication.class, args);
	}

	@GetMapping("/hello")
	public String hello(@RequestParam(value = "name", defaultValue = "World") String name) {
		return String.format("Hello %s!", name);
	}

	@GetMapping("/new1")
	public ArrayList<Integer> new1(@RequestParam(value = "count") Integer c) {
		Random rnd = new Random();
		ArrayList<Integer> a = new ArrayList<Integer>();
		for(int i = 0; i< c; i++){
			a.add(rnd.nextInt() % 10);
		}

		return a;
	}
}
